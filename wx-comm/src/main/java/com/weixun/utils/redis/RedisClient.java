package com.weixun.utils.redis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.List;
import java.util.ResourceBundle;

/**
 * 
 * <p>
 * 	Redis客户端访问
 * </p>
 * 
 * @author myd
 * @创建时间：2016年7月11日
 * @version： V1.0
 */
public class RedisClient {
	
	public  static JedisPool jedisPool; // 池化管理jedis链接池
	
	static {
		
		//读取相关的配置
		ResourceBundle resourceBundle = ResourceBundle.getBundle("redis");
		int maxTotal = Integer.parseInt(resourceBundle.getString("redis.maxTotal"));
//		int maxActive = Integer.parseInt(resourceBundle.getString("redis.maxActive"));
		int maxIdle = Integer.parseInt(resourceBundle.getString("redis.maxIdle"));
		int maxWaitMillis = Integer.parseInt(resourceBundle.getString("redis.maxWaitMillis"));
		int timeout = Integer.parseInt(resourceBundle.getString("redis.timeout"));
		int port = Integer.parseInt(resourceBundle.getString("redis.port"));

		String ip = resourceBundle.getString("redis.host");

		String password = resourceBundle.getString("redis.password");
		JedisPoolConfig config = new JedisPoolConfig();
		//设置最大连接数
//		config.setMaxTotal(maxTotal);
//		//设置最大空闲数
//		config.setMaxIdle(maxIdle);
//		//设置超时时间
//		config.setMaxWaitMillis(maxWaitMillis);
		//初始化连接池
		jedisPool = new JedisPool(config, ip, port,timeout,password);
		Jedis jedis = jedisPool.getResource();
		String res = jedis.ping();

	}
	
	/**
	 * 向缓存中设置字符串内容
	 * @param key key
	 * @param value value
	 * @return
	 * @throws Exception
	 */
	public static boolean  set(String key,String value) throws Exception{
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.set(key, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally{
			//关闭jedis连接
			if (jedis != null) {
				jedisPool.close();
			}
		}
	}
	
	/**
	 * 向缓存中设置对象
	 * @param key 
	 * @param value
	 * @return
	 */
	public static boolean  set(String key,Object value){
		Jedis jedis = null;
		try {
			String objectJson = JSON.toJSONString(value);
			jedis = jedisPool.getResource();
			jedis.set(key, objectJson);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally{
			if (jedis != null) {
				jedisPool.close();
			}
		}
	}

	/**
	 * 删除缓存中得对象，根据key
	 * @param key
	 * @return
	 */
	public static boolean del(String key){
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.del(key);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally{
			if (jedis != null) {
				jedisPool.close();
			}
		}
	}
	
	/**
	 * 根据key 获取内容
	 * @param key
	 * @return
	 */
	public static Object get(String key){
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			Object value = jedis.get(key);
			return value;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally{
			if (jedis != null) {
				jedisPool.close();
			}
		}
	}
	

	/**
	 * 根据key 获取对象
	 * @param key
	 * @return
	 */
	public static <T> T get(String key,Class<T> clazz){
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			String value = jedis.get(key);
			return JSON.parseObject(value, clazz);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if (jedis != null) {
				jedisPool.close();
			}
		}
	}

	/**
	 * 更据key获取list对象
	 * @param key
	 * @param clazz
	 * @param <T>
     * @return
     */
	public static  <T> T getlist(String key,Class<T> clazz)
	{
		Jedis jedis = null;
		List<T> list = null;
		try {
			jedis = jedisPool.getResource();
			String  json = jedis.get(key);
			list = JSONArray.parseArray(json,clazz);
			return (T) list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			if (jedis != null) {
				jedisPool.close();
			}
		}
	}



}
